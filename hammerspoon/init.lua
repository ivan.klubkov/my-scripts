screen = { w = 1920, h = 1080}

terminals = {}

LastApp = nil

function reloadConfig(files)
    doReload = false
    for _,file in pairs(files) do
        if file:sub(-4) == ".lua" then
            doReload = true
        end
    end
    if doReload then
        hs.reload()
    end
end
myWatcher = hs.pathwatcher.new(os.getenv("HOME") .. "/.hammerspoon/", reloadConfig):start()
hs.alert.show("Config loaded!")

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "I", function()
local win = hs.window.focusedWindow()
local f = win:frame()

f.x = screen.w
f.y = 0
f.w = screen.w / 2
f.h = screen.h / 2
win:setFrame(f)
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "O", function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    
    f.x = screen.w+screen.w/2
    f.y = 0
    f.w = screen.w/2
    f.h = screen.h/2
    win:setFrame(f)
    end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "K", function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    
    f.x = screen.w
    f.y = screen.h/2
    f.w = screen.w/2
    f.h = screen.h/2
    win:setFrame(f)
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "[", function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    
    f.x = screen.w
    f.y = 0
    f.w = screen.w/2
    f.h = screen.h
    win:setFrame(f)
end)
    
hs.hotkey.bind({"cmd", "alt", "ctrl"}, "]", function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    
    f.x = screen.w + screen.w / 2
    f.y = 0
    f.w = screen.w/2
    f.h = screen.h
    win:setFrame(f)
end)


hs.hotkey.bind({"cmd", "alt", "ctrl"}, "L", function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    
    f.x = screen.w+screen.w/2
    f.y = screen.h/2
    f.w = screen.w/2
    f.h = screen.h/2
    win:setFrame(f)
    end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "0", function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    
    if f.x < screen.w then
        f.x = 0
    else
        f.x = screen.w
    end
    f.y = 0
    f.w = screen.w
    f.h = screen.h
    win:setFrame(f)
end)

app1 = nil
app2 = nil
app3 = nil
app4 = nil
app5 = nil

hs.hotkey.bind({"cmd", "alt", "ctrl", "shift"}, '1', function()
    app1 = hs.window.focusedWindow()
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, '1', function()
    if app1 ~= nil then
        app1:focus()
    end
end)

hs.hotkey.bind({"cmd", "alt", "ctrl", "shift"}, '2', function()
    app2 = hs.window.focusedWindow()
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, '2', function()
    if app2 ~= nil then
        app2:focus()
    end
end)

hs.hotkey.bind({"cmd", "alt", "ctrl", "shift"}, '3', function()
    app3 = hs.window.focusedWindow()
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, '3', function()
    if app3 ~= nil then
        app3:focus()
    end
end)

hs.hotkey.bind({"cmd", "alt", "ctrl", "shift"}, '4', function()
    app4 = hs.window.focusedWindow()
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, '4', function()
    if app4 ~= nil then
        app4:focus()
    end
end)

hs.hotkey.bind({"cmd", "alt", "ctrl", "shift"}, '5', function()
    app5 = hs.window.focusedWindow()
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, '5', function()
    if app5 ~= nil then
        app5:focus()
    end
end)



hs.hotkey.bind({"cmd", "alt", "ctrl"}, 'V', function()
    hs.application.launchOrFocus("Vox")
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, 'S', function()
    local app = hs.window.focusedWindow():application()
    if app:name() == "Slack" and LastApp ~= nil then
        LastApp:activate()
    else
        LastApp = app
        hs.application.launchOrFocus("Slack")
    end
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, 'T', function()
    hs.application.launchOrFocus("Terminal")
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, 'N', function()
    hs.application.launchOrFocus("Терминал")
    local terminal = hs.appfinder.appFromName("Терминал")
    terminal:selectMenuItem({"Shell", "Новое окно", "Новое окно с профилем*"})
end)


-- --- ====   E X P E R I M E N T A L   S E C T I O N   ==== --- --

hs.hotkey.bind({"cmd", "alt", "ctrl"}, 'Q', function()
    hs.application.launchOrFocus("Терминал")
    local terminal = hs.appfinder.appFromName("Терминал")
    terminal:selectMenuItem({"Вид", "Уменьшить"})
    terminal:selectMenuItem({"Вид", "Уменьшить"})
    terminal:selectMenuItem({"Вид", "Уменьшить"})
    terminal:selectMenuItem({"Вид", "Уменьшить"})
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, 'W', function()
    hs.application.launchOrFocus("Терминал")
    local terminal = hs.appfinder.appFromName("Терминал")
    terminal:selectMenuItem({"Вид", "Размер шрифта по умолчанию"})
end)


watcher = nil

function termWatcher(element, event, watcher, userData)
    local win = nil
    if element:isApplication() then
        win = element:focusedWindow()
    else
        win = element
    end

    hs.alert.show("event!")

    local f = win:frame()

    if f.x < screen.w then
        f.x = 0
    else
        f.x = screen.w
    end
    f.y = 0
    f.w = screen.w
    f.h = screen.h
    win:setFrame(f)
end

-- term = hs.appfinder.appFromName("Terminal")
-- if term == nil then
--     term = hs.appfinder.appFromName("Терминал")
-- end

-- if term ~= nil then
--     watcher = term:newWatcher(termWatcher)
--     if watcher ~= nil then
--         hs.alert.show("Term hooked!")    
--         watcher:start({hs.uielement.watcher.focusedWindowChanged, hs.uielement.watcher.focusedElementChanged, 
--         hs.uielement.watcher.windowCreated, hs.uielement.watcher.applicationActivated})
--     end  
-- end


-- hs.application.watcher.new(function(name, event, application)
--     if name == 'Terminal' or name == 'Терминал' then
--         if (event == hs.application.watcher.launched) then
--             watcher = application:newWatcher(termWatcher)
--             if watcher ~= nil then
--                 hs.alert.show("Term hooked!")    
--                 watcher:start({hs.uielement.watcher.focusedWindowChanged, hs.uielement.watcher.focusedElementChanged, 
--                 hs.uielement.watcher.windowCreated, hs.uielement.watcher.applicationActivated})
--             end                    
--         elseif (event == hs.application.watcher.terminated) then
--             if watcher ~= nil then
--                 watcher:stop()
--                 watcher = nil
--                 hs.alert.show("Term unhooked!")    
--             end
--         end
--     end
-- end)



