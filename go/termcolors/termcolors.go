package termcolors

// Termcolors library

import (
	"strconv"
	"strings"
)

var COLOR = map[string]int{
	"BLACK":   0,
	"RED":     1,
	"GREEN":   2,
	"YELLOW":  3,
	"BLUE":    4,
	"MAGENTA": 5,
	"CYAN":    6,
	"WHITE":   7,
}

const FORE int = 30
const BACK int = 40
const LIGHT int = 60

func Color(definition ...string) string {
	var result string
	var value int = -1
	var bold bool = false
	var back bool = false
	var light bool = false
	for _, key := range definition {
		key = strings.ToUpper(key)
		//fmt.Println(key)
		switch key {
		case "OFF":
			return "\033[0m"
		case "BOLD":
			bold = true
		case "LIGHT":
			light = true
		case "BACK":
			back = true
		default:
			N, ok := COLOR[key]
			if ok {
				value = N
			}
		}
	}
	if bold {
		result += "\033[1m"
	}
	if value >= 0 {
		if back {
			value += BACK
		} else {
			value += FORE
		}
		if light {
			value += LIGHT
		}
		result += "\033[" + strconv.Itoa(value) + "m"
	}
	return result
}
