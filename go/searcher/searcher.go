package main

/* searcher with regulars v.1.0 */

import (
	"bufio"
	"flag"
	"fmt"
	"my/termcolors"
	"os"
	"path/filepath"
	"regexp"
	s "strings"
)

var root string
var file string
var pathPattern string
var searchPattern string
var searchPatternMatcher *regexp.Regexp
var multiline bool

var COLOR_HEAD string = termcolors.Color("LIGHT", "YELLOW", "BOLD")
var COLOR_BOLD string = termcolors.Color("BOLD")
var COLOR_ON string = termcolors.Color("LIGHT", "CYAN")
var COLOR_OFF string = termcolors.Color("OFF")

type ItemInfo = struct {
	line int
	from int
	to   int
}
type IntArray = []int16
type LineInfo = []int

type Line = struct {
	n int
	s string
}
type LineArray = []Line

func countLeftSpaces(s string) int {
	var i int = 0
	var n int = 0
	for i < len(s) {
		if s[i] == ' ' {
			i++
			n++
		} else if s[i] == '#' {
			i++
		} else {
			break
		}
	}
	if i == len(s) {
		return 6000
	} else {
		return n
	}
}

func findInSlice(lines []Line) []ItemInfo {
	var results []ItemInfo
	for _, line := range lines {
		found := searchPatternMatcher.FindAllStringIndex(line.s, -1)
		if found != nil {
			for _, founded := range found {
				results = append(results, ItemInfo{line: line.n, from: founded[0], to: founded[1]})
			}
		}
	}
	if len(results) == 0 {
		return nil
	}
	return results
}

func findInSliceMultiline(lines []Line) []ItemInfo {
	var results []ItemInfo
	var line string = ""
	var indexes []int

	for _, str := range lines {
		indexes = append(indexes, len(line))
		line = line + str.s
	}

	// found := searchPatternMatcher.FindAllStringIndex(line, -1)
	// if found != nil {
	// 	fromLine := 0
	// 	for _, match := range found {
	// 		from, to := match[0], match[1]
	// 		for from < indexes[fromLine] {
	// 			fromLine++
	// 		}
	// 		toLine := fromLine
	// 		for to < indexes[toLine] {
	// 			toLine++
	// 		}
	// 		if fromLine == toLine {
	// 			results = append(results, ItemInfo{line: first + fromLine, from: from - indexes[fromLine], to: to - indexes[toLine]})
	// 		} else {
	// 			results = append(results, ItemInfo{line: first + fromLine, from: from - indexes[fromLine], to: -1})
	// 			results = append(results, ItemInfo{line: first + toLine, from: -1, to: to - indexes[toLine]})
	// 		}
	// 		fromLine = toLine
	// 	}
	// }
	// if len(results) == 0 {
	// 	return nil
	// }
	return results
}

func index(lines LineArray, n int) int {
	for i, l := range lines {
		if l.n == n {
			return i
		}
	}
	return -1
}

func indexLess(lines LineArray, n int) int {
	for i, l := range lines {
		if l.n == n {
			return i
		} else if l.n > n {
			return i - 1
		}
	}
	return len(lines) - 1
}

func slice(lines LineArray, from int, to int) LineArray {
	return lines[indexLess(lines, from):indexLess(lines, to)]
}

func printItem(lines []Line, searchType string, hits []ItemInfo) {
	if searchType == "compressed" {
		// fmt.Println(">>>", lines[0].n, len(lines), hits)
		for i := len(hits) - 1; i >= 0; i-- {
			var n = index(lines, hits[i].line)
			if hits[i].to != -1 {
				lines[n].s = lines[n].s[:hits[i].to] + COLOR_OFF + lines[n].s[hits[i].to:]
			}
			if hits[i].from != -1 {
				lines[n].s = lines[n].s[:hits[i].from] + COLOR_ON + lines[n].s[hits[i].from:]
			}
		}

		fmt.Printf("\n%5d %s\n", lines[0].n+1, lines[0].s)
		last := 0
		for _, hit := range hits {
			var n = index(lines, hit.line)
			if n > last {
				last = n
				fmt.Printf("%5d %s\n", lines[last].n+1, lines[last].s)
			}
		}
	}
}

func ignored(line string) bool {
	line = s.TrimSpace(line)
	if len(line) == 0 || line[0] == '#' {
		return true
	}
	return false
}

func readLines(path string) ([]Line, [][2]int, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, nil, err
	}
	defer file.Close()

	var lines []Line
	var items [][2]int
	var line string
	scanner := bufio.NewScanner(file)
	n := 0
	start := 0
	last := 0
	for scanner.Scan() {
		line = scanner.Text()
		lines = append(lines, Line{n: n, s: line})

		current := countLeftSpaces(line)
		if n > start && current == 0 && current < last && len(s.TrimSpace(line)) > 0 && s.TrimSpace(line)[0] != '#' {
			//			fmt.Println(">>>>>", start, n-1)
			items = append(items, [2]int{start, n - 1})
			start = n
		}
		last = current
		n += 1
	}
	items = append(items, [2]int{start, n - 1})

	return lines, items, scanner.Err()
}

func searchInFile(filename string, searchType string, colorify bool) string {
	lines, items, err := readLines(filename)
	var result string
	var printHeader bool = true
	var founded []ItemInfo

	if err == nil {
		//		printFile(lines)
		for _, item := range items {
			if multiline {
				founded = findInSliceMultiline(slice(lines, item[0], item[1]))
			} else {
				founded = findInSlice(slice(lines, item[0], item[1]))
			}
			if founded != nil {
				if printHeader {
					fmt.Printf("\n%s%s  --- %s ---%s\n\n", COLOR_HEAD, COLOR_BOLD, filename, COLOR_OFF)
					printHeader = false
				}
				printItem(slice(lines, item[0], item[1]), "compressed", founded)
			}
		}
	} else {
		fmt.Println("Error reading file", filename)
	}

	return result
}

func printFile(lines []Line) {
	for _, l := range lines {
		fmt.Printf("%5d  %s\n", l.n, l.s)
	}
}

func init() {
	flag.StringVar(&root, "path", ".", "Path where to search for files")
	flag.StringVar(&file, "file", "", "File name to analyse")
	flag.StringVar(&pathPattern, "pathpattern", ".*", "Pattern for matching files")
	flag.StringVar(&searchPattern, "pattern", "", "Pattern to search for in files")
	flag.BoolVar(&multiline, "multiline", false, "Use multiline search pattetn matching")
}

func main() {

	flag.Parse()

	if searchPattern == "" {
		flag.PrintDefaults()
		return
	}

	pathMatcher := regexp.MustCompile(pathPattern)
	searchPatternMatcher = regexp.MustCompile(searchPattern)

	if len(file) > 0 {
		// single file
		fmt.Println("Single file ", file)
		searchInFile(file, "full", true)
	} else {
		// multiple files
		var files []string
		fmt.Println("Multiple files @", root, ": ")

		err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
			if info != nil && !info.IsDir() && (pathPattern == ".*" || pathMatcher.MatchString(path)) {
				files = append(files, path)
			}
			return nil
		})
		if err != nil {
			panic(err)
		}
		if len(files) == 0 {
			fmt.Println("No files found in", root)
		}
		for _, file := range files {

			searchInFile(file, "full", true)
		}
	}
}
