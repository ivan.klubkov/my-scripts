#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import getopt
import sys
import os

# Global variables (sic!) for configuration. Main code fills options gathered from command line here
cache = []
ignore_case = False
expression = ""
wants_help = False
print_type = 'borders'
first = True
show_commented = False
colorify = 255
maxwidth = 65535

# terminal escape command colors
# ToDo: Add other codes here
palette = {
    0: 'OFF',
    1: 'BOLD',
    90: 'GREY',
    91: 'RED',
    92: 'GREEN',
    93: 'YELLOW',
    94: 'BLUE',
    95: 'PURPLE',
    96: 'CYAN',
    97: 'WHITE'
}

# colors for interleaving found text
print_colors = ['OFF', 'WHITE', 'CYAN']


# Returns string with escape command for changing text color in terminal
def painter(color: str) -> str:
    for i in palette.keys():
        if palette[i] == color.upper():
            return '\033[%dm' % i
    return ''


# gets and applies color escape command
def set_print_color(color: str):
    print(painter(color), end='')


# prints data in specified color
# color scheme is passed in named argumnt "color="
# examples:
#   color="YELLOW.BOLD"
#   color="BLUE"
#   color="OFF"
def color_print(*arguments, **named):
    colors = []
    if 'color' in named.keys():
        colors = named['color'].split('.')
    for color in colors:
        set_print_color(color)
    for elem in arguments:
        print(elem, end=' ')
    for _ in colors:
        set_print_color('OFF')
    print()


# checks wether this line must be skipped in output
# empty lines (always) and comments (by default) are skipped
def is_skipped(s: str):
    return len(s.strip()) == 0 or not show_commented and len(s.strip()) > 0 and s.strip()[0] == '#'


# count indentation of the string (number of leading spaces)
def count_left_spaces(s: str):
    n = 0
    while n < len(s) and s[n].isspace():
        n += 1
    return n


# prints line number, then string s with wrapping, preserving line's indentation
# wrapping occurs on maxwidth width
def print_indented(n: int, s: str):
    indent = count_left_spaces(s) + 6   # 6 for leading line number
    w = maxwidth - indent - 1
    print('%4d:' % n, s[:w-5])
    i = w-5
    while i < len(s):
        print("%s%s" % (' '*indent, s[i:(i+w+1)]))
        i += w


# prints range [a,b] of the cache with line numbers.
# Only unneded lines are skipped
def print_range(a, b):
    for i in range(a, b):
        if is_skipped(cache[i]):
            continue
        print_indented(i+1, cache[i][:-1])


# prints range [a,b] of the cache collapsed by indentation.
# only last line of each indentation level preceding found value line (last line) are printed
# unneded lines skipped
#
# runs over [a,b] of the cache range BACKWARDS and moves line numbers of the lines with indentation
# lower then current in a temprary list, then reverses the list and prints cache lines
def print_collapsed(a, b):
    level = 1000
    to_print = []
    for i in range(b-1, a-1, -1):
        if is_skipped(cache[i]):
            continue
        current = count_left_spaces(cache[i])
        if current < level:
            to_print.append(i)
            level = current
    to_print.reverse()
    for i in range(len(to_print)):
        print_indented(to_print[i], cache[to_print[i]][:-1])


# prints only borders of a range in cache (a and b-1)
def print_borders(a, b):
    print_indented(a+1, cache[a][:-1])
    print_indented(b, cache[b-1][:-1])


# search result presenter
# analyses in which global object current line (pos) is, by walking "up" until upper level
# (with indentation = 0) found. Then prints found range according to settings
# When invoked for the first time prints file name
# additionally adds coloring to range (from print_colors array) and found value ('PURPLE')
# ToDo: change colors to suit both dark and light terminal schemes
def show(pos: int, info):
    global first, colorify

    current_painter = painter("OFF")

    # colorify += 1
    # if colorify >= len(print_colors):
    #     colorify = 0
    # current_painter = painter(print_colors[colorify])
    # print(current_painter, end='')

    if 0 <= pos < len(cache):

        if is_skipped(cache[pos]):
            return

        to = pos+1
        while pos >= 0 and (cache[pos][0].isspace() or
                            cache[pos][0] == '#' and cache[pos][1].isspace()):
            pos -= 1

        info.reverse()

        for b, e in info:
            cache[to-1] = cache[to-1][:b] + painter('PURPLE') + \
                          cache[to-1][b:e] + current_painter + \
                          cache[to-1][e:]

        if print_type == 'ranges':
            print_range(pos, to)
        elif print_type == 'collapsed':
            print_collapsed(pos, to)
        elif print_type == 'borders':
            print_borders(pos, to)
        print()


# search result presenter
# analyses in which global object current line (pos) is, by walking "up" until upper level
# (with indentation = 0) found. Then prints found range according to settings
# When invoked for the first time prints file name
# additionally adds coloring to range (from print_colors array) and found value ('PURPLE')
# ToDo: change colors to suit both dark and light terminal schemes
def show_1(pos, to, results):
    global first, colorify

    colorify += 1
    if colorify >= len(print_colors):
        colorify = 0
    current_painter = painter(print_colors[colorify])
    print(current_painter, end='')

   #print(results)

    for found in results:
            i = found[0]
            res = found[1]
            res.reverse()
            for b, e in res:
                cache[i] = cache[i][:b] + painter('PURPLE') + \
                              cache[i][b:e] + current_painter + \
                              cache[i][e:]
            end = i+1

    if print_type == 'ranges':
        print_range(pos, end)
    elif print_type == 'collapsed':
        print_collapsed(pos, end)
    elif print_type == 'borders':
        print_borders(pos, to)
    print()


# search for a pattern in a file
def do_search(files):
    global cache
    for file_name in files:
        try:
            with open(file_name, 'r', errors='replace') as f:
                cache = f.readlines()
        except FileNotFoundError:
            color_print("ERROR: file", file_name, "not found.", color='RED')
            return

        if ignore_case:
            search = re.compile(expression, re.IGNORECASE)
        else:
            search = re.compile(expression)

        n = 0
        start = 0
        last = 0
        results = []
        items = []
        for line in cache:

#            if is_skipped(line):
#                continue

            current = count_left_spaces(line)
            if start < n and current == 0 and current < last and \
               len(line.strip()) > 0 and line.strip()[0] != '#':
                items.append((start, n-1))
                start = n
            last = current

            iterator = search.finditer(line)
            found = []
            for i in iterator:
                found.append(i.span())
            if len(found) > 0:
                results.append((n, found))
            n += 1
        items.append((start, n - 1))

        if len(results) == 0:
            continue

        print()
        color_print("--- FILE:", file_name, "---", color="YELLOW.BOLD")
        #       print(results, items)
        print()

        for found in results:
            show(found[0], found[1])

  #       item_n = 0
  #       found_n = 0
  #
  #       while True:
  #           while item_n < len(items) and results[found_n][0] > items[item_n][1]:
  #               item_n += 1
  #           if item_n >= len(items):
  #               break
  # #          print(items[item_n], results[found_n])
  #           tmp = []
  #           while found_n < len(results) and items[item_n][0] <= results[found_n][0] <= items[item_n][1]:
  #               tmp.append(results[found_n])
  #               found_n += 1
  #           if len(tmp) > 0:
  #               show_1(items[item_n][0], items[item_n][1], tmp)
  #           if found_n >= len(results):
  #               break


def get_all_files(directory):
    found = []
    for root, subdirs, files in os.walk(directory):
        for file_name in files:
            found.append(os.path.join(root,file_name))
    return found


if __name__ == "__main__":
    optlist, args = getopt.getopt(sys.argv[1:], "hcipr:s:w:d:", ["help", "commented", "print-type=", "ignorecase", "regular=", "string=", "width=", "dir="])

    directory = "."
    for option, value in optlist:
        if option == '-h' or option == '--help':
            wants_help = True
        elif option == "-c" or option == '--commented':
            show_commented = True
        elif option == "-i" or option == '--ignorecase':
            ignore_case = True
        elif option == "-p" or option == '--print-type':
            print_type = value
        elif option == "-s" or option == '--string':
            expression = value
        elif option == "-r" or option == '--regular':
            expression = value
        elif option == "-w" or option == '--width':
            maxwidth = value
        elif option == "-d" or option == '--dir':
            directory = value

    if wants_help or len(args) != 1 and directory == ".":
        print("""
            Use the Force
        """)
        sys.exit(0)

    if len(args) == 1:
        files_list = [args[0]]
    else:
        files_list = get_all_files(directory)

    if len(files_list) == 0:
        print("ERROR: No files, directory is not accessible or empty")

    do_search(files_list)

    print("TOTAL: ", len(files_list), "files checked")